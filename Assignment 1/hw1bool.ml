(* Problem 2.1 *)
type bool_expr = 
  | Lit of string
  | Not of bool_expr
  | And of bool_expr * bool_expr
  | Or of bool_expr * bool_expr

let rec simplify lit_one lit_one_value lit_two lit_two_value expression =
  match expression with
  | Lit expr -> if expr = lit_one then lit_one_value else lit_two_value
  | Not expr -> not(simplify lit_one lit_one_value lit_two lit_two_value expr)
  | And (expr1, expr2) -> (simplify lit_one lit_one_value lit_two lit_two_value expr1) && (simplify lit_one lit_one_value lit_two lit_two_value expr2)
  | Or (expr1, expr2) -> (simplify lit_one lit_one_value lit_two lit_two_value expr1) || (simplify lit_one lit_one_value lit_two lit_two_value expr2)

let truth_table lit_one lit_two expression =
  [(true, true, simplify lit_one true lit_two true expression);
   (true, false, simplify lit_one true lit_two false expression);
   (false, true, simplify lit_one false lit_two true expression);
   (false, false, simplify lit_one false lit_two false expression)]