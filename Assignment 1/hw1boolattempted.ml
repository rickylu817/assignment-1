type bool_expr = 
  | Lit of string
  | Not of bool_expr
  | And of bool_expr * bool_expr
  | Or of bool_expr * bool_expr

let rec reverse list reversed =
  match list with
  | [] -> reversed
  | first::rest -> reverse rest (first::reversed);;
  
let rec evaluate_not list acc =
  match list with
  | [] -> acc
  | (a,b,true)::rest -> evaluate_not rest ((a,b,false)::acc)
  | (a,b,false)::rest -> evaluate_not rest ((a,b,true)::acc)

let rec evaluate_and list acc =
  match list with
  | [] -> acc
  | (true,true,_)::rest -> evaluate_and rest ((true,true,true)::acc)
  | (true,false,_)::rest -> evaluate_and rest ((true,false,false)::acc)
  | (false,true,_)::rest -> evaluate_and rest ((false,true,false)::acc)
  | (false,false,_)::rest -> evaluate_and rest ((false,false,false)::acc)

let rec evaluate_or list acc =
  match list with
  | [] -> acc
  | (true,true,_)::rest -> evaluate_or rest ((true,true,true)::acc)
  | (true,false,_)::rest -> evaluate_or rest ((true,false,true)::acc)
  | (false,true,_)::rest -> evaluate_or rest ((false,true,true)::acc)
  | (false,false,_)::rest -> evaluate_or rest ((false,false,false)::acc)

exception Ohno of string
let get_rest list = 
  match list with
  | [] -> []
  | first::rest -> rest

let get_tail list =
  match list with
  | first::[] -> first
  | _ -> raise (Ohno "Error");;

let rec joinab list1 list2 acc =
  match list1 with
  | [] -> acc
  | first::[] -> (first::(get_tail list2)::[])
  | first::rest -> joinab rest (get_rest list2) [];;

let truth_table lit_one lit_two expression = 
  let rec truth_table_helper lit_one lit_two expression acc =
    match expression with
    | Lit expr -> if expr = lit_one then [(true,false,true);(true,false,true);(false,false,false);(false,false,false)]
       else [(true,false,true);(false,false,false);(true,false,true);(false,false,false)]
    | Or (expr1, expr2) -> evaluate_or (joinab (truth_table_helper lit_one lit_two expr1 acc) (truth_table_helper lit_one lit_two expr2 acc) []) []
    | And (expr1, expr2) -> evaluate_and (joinab (truth_table_helper lit_one lit_two expr1 acc) (truth_table_helper lit_one lit_two expr2 acc) []) []
    | Not expr -> evaluate_not (truth_table_helper lit_one lit_two expr []) []  in
  reverse (truth_table_helper lit_one lit_two expression []) [];;
