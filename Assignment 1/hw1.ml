(* Problem 1*)
(* This is the int version of power function *)
let rec pow x n = 
  match n with
  | 0 -> 1
  | _ -> x * pow x (n-1);;

(* This is the float version of the power function *)
let rec float_pow x n =
  match n with
  | 0 -> 1.0
  | _ -> x *. float_pow x (n-1);;

(* Problem 2 *)
(* Scratch this off since I can't use List.hd)
let compress list =
  match list with
  | [] -> []
  | _ -> let rec compress_helper list acc prev =
          match list with
          | [] -> acc
          | h::t -> if prev = h then compress_helper t acc prev else compress_helper t (h::acc) h in

          let rec rev thelist reverse =
            match thelist with
            | [] -> reverse
            | first::rest -> rev rest (first::reverse) in
            rev (compress_helper list [List.hd list] (List.hd list)) [];;
*)
let rec reverse list reversed =
  match list with
  | [] -> reversed
  | first::rest -> reverse rest (first::reversed);;

let compress list =
  let rec compress_helper list acc = 
    match list with 
    | first::second::rest -> if first = second then compress_helper (second::rest) acc else compress_helper (second::rest) (first::acc)
    | first::rest -> compress_helper [] (first::acc)
    | [] -> acc in
  reverse (compress_helper list []) [];;

(* Problem 3 *)
(* This method is really cool because it can take in a function as an input *)
let remove_if list f =
  let rec remove_if_helper list f acc =
    match list with
    | [] -> acc
    | first::rest -> if f first = false then remove_if_helper rest f (first::acc) else remove_if_helper rest f acc in
  reverse (remove_if_helper list f []) [];;

(* Problem 4 *)
let rec index_it list n indexedList =
  match list with 
  | [] -> indexedList
  | first::rest -> index_it rest (n+1) ((n , first)::indexedList);;

let rec length_of list counter =
  match list with
  | [] -> counter
  | first::rest -> length_of rest counter+1;;

let slice list i j =
  if i >= j then [] else let rec slice_helper list i j acc  =
    match list with 
    | [] -> acc
    | (x,y)::rest -> if x >= i && x <j then slice_helper rest i j (y::acc) else slice_helper rest i j acc in
  slice_helper (index_it list 0 []) i j [];; 

(* Problem 5 *) (*
let rec distribute f list comparing acc =
  match list with
  | [] -> (comparing::[])::acc
  | first::rest -> if (f first comparing) then ((comparing::first)::rest) else distribute f rest comparing acc
*)
exception Ohno of string
let get_head list = 
  match list with
  | first::rest -> first
  | _ -> raise (Ohno "Error");;

let rec flip_inner_list list acc =
  match list with
  | [] -> acc
  | first::rest -> flip_inner_list rest ((reverse first [])::acc)

let rec distribute2 f list comparing acc =
  match list with 
  | [] -> (comparing::[])::acc
  | first::rest -> if (f (get_head first) comparing) then ((comparing::first))::rest else (first::(distribute2 f rest comparing []))
let equivs f list =
  let rec equivs_helper f list actualAnswer =
    match list with
    | [] -> actualAnswer
    | first::rest -> equivs_helper f rest (distribute2 f actualAnswer first actualAnswer) in
  flip_inner_list(reverse (equivs_helper f list []) []) [];;

(* Problem 6 *)
let rec is_prime x d =
  if x = d then true else
    if x mod d = 0 then false else
      if 2*d > x then true else 
        is_prime x (d+1);;

let goldbachpair x =
  let rec goldbachpair_helper x testing =
    if is_prime testing 2 then if is_prime (x-testing) 2 then (testing::((x-testing)::[])) else 
    goldbachpair_helper x (testing+1) else goldbachpair_helper x (testing+1) in
  goldbachpair_helper x 2;;

(* Problem 7 *)
let rec check_f f list acc = 
  match list with
  | [] -> acc
  | first::rest -> check_f f rest (f first::acc);;

let rec check_g g list acc = 
  match list with
  | [] -> acc
  | first::rest -> check_g g rest (g first::acc);;

let equiv_on f g list =
  if check_f f list [] = check_g g list [] then true else false;;

(* Problem 8 *)
let pairwisefilter cmp list =
  let rec pairwise_helper cmp list acc =
    match list with
    | [] -> acc
    | first::second::rest -> pairwise_helper cmp rest (cmp first second::acc)
    | first::rest -> pairwise_helper cmp rest (first::acc) in
  reverse (pairwise_helper cmp list []) [];;

(* Problem 9 *)(* Scrach this off 
let polynomial list = 
  let rec polynomial_helper list = 
    match list with
    | (coe,power)::rest -> fun x -> coe*(pow x power) + helper rest and helper list = polynomial_helper list 
    | _ -> fun x -> 0 in
  polynomial_helper list;;
*)
let rec translate list x =
  match list with 
  | (coe,power)::rest -> coe*(pow x power) + translate rest x
  | [] -> 0;;

let polynomial list x =
  translate list x;;

(* Problem 10 *)
let rec put_me_inside list me acc=
  match list with
  | first::rest -> put_me_inside rest me (reverse (me::(reverse first [])) []::acc)
  | _ -> acc;;
let powerset list =
  let rec powerset_helper list actualAnswer =
    match list with
    | first::rest -> powerset_helper rest (put_me_inside actualAnswer first actualAnswer)
    | _ ->  reverse actualAnswer [] in
  powerset_helper list [[]];;
