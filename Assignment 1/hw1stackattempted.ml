(* Problem 2.2 *)
let start x = x [];;

let halt x = x;;

let pop stack another_operation =
  match stack with
  | [] -> another_operation []
  | first::rest -> another_operation rest;;

exception EmptyStack of string
let peek stack =
  match stack with
  | first::rest -> first
  | _ -> raise (EmptyStack ("Empty Stack"));;

let push n stack another_operation = another_operation (n::stack);;

let clone stack another_operation = another_operation ((peek stack)::stack);;

let add stack another_operation =
  match stack with
  | num1::num2::rest -> another_operation ((num1+num2)::rest)
  | _ -> another_operation stack;;
let mult stack another_operation = 
  match stack with
  | num1::num2::rest -> another_operation ((num1*num2)::rest)
  | _ -> another_operation stack;;

