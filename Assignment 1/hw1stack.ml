type stack =
  | Another of int list;;

let start x = x (Another ([]));;

let halt x =
  match x with
  | Another (list) -> list;;

exception EmptyStack of string
let peek stack =
  match stack with
  | Another (first::rest) -> first
  | _ -> raise (EmptyStack ("Empty Stack"));;

let pop stack another_operation =
  match stack with
  | Another([]) -> another_operation (Another([]))
  | Another (first::rest) -> another_operation (Another(rest));;

let kpop stack another_operation =
  let rec kpop_helper n stack =
    match stack with 
    | Another (first::rest) -> if n > 0 then kpop_helper (n-1) (Another (rest)) else stack
    | _ -> stack in
  if (peek stack) < 0 then another_operation (kpop_helper (1) stack) else 
  another_operation (kpop_helper ((peek stack)+1) stack);;

let push n stack another_operation =
  match stack with
  | Another (first::rest) -> another_operation (Another(n::first::rest))
  | _ -> another_operation (Another (n::[]));;

let clone stack another_operation =
  match stack with
  | Another (first::rest) -> another_operation (Another(first::first::rest))
  | _ -> another_operation (Another ([]));;

let add stack another_operation =
  match stack with
  | Another (num1::num2::rest) -> another_operation (Another ((num1+num2)::rest))
  | _ -> another_operation stack;;

let mult stack another_operation = 
  match stack with
  | Another (num1::num2::rest) -> another_operation (Another((num1*num2)::rest))
  | _ -> another_operation stack;;
  
  

